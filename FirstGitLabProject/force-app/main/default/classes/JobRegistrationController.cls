public class JobRegistrationController {

public  Job_Shubham__c Job { get; private set; }
 
  public JobRegistrationController (){
     
         Id id =ApexPages.currentPage().getParameters().get('id');
         Job= (id==null)?new Job_Shubham__c():[select Name__c,Description__c,Job_Types1__c,   Number_of_Positions__c,Salary_Offered__c, Expires_On__c,Active__c,Qualification_Required__c   ,Required_Skills__c,Certification_Required__c,   Hired_Applicants__c,Total_Applicant__c,Manager__c from Job_Shubham__c  where Id=:id];
  }
  

    public PageReference save() {
         try{
              upsert(Job);
       }
       catch(System.DMLexception e){
     
           ApexPages.addMessages(e);
           return null;
       }
     
       PageReference pr= new ApexPages.standardController(Job).view();
       return pr;
   }
    }