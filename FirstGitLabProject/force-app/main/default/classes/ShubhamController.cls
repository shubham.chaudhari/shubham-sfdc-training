public class ShubhamController {
public Candidate_Shubham__c candidates{get; private set;}
   
   public ShubhamController()
   {
          Id id =ApexPages.currentPage().getParameters().get('id');
     candidates= (id==null)?new Candidate_Shubham__c():[select First_Name__c,Last_Name__c,DOB__c,Email__c,Expected_Salary__c,Country__c,State__c,Status__c,Job_Shubham__c ,Application_Date__c,Name from Candidate_Shubham__c where Id=:id];
   }
   
   public PageReference save(){
      try{
          upsert(Candidates);
      }
      catch(System.DMLException e)
      {
          ApexPages.addMessages(e);
          return null;
      }
       PageReference redirectSuccess = new ApexPages.StandardController(candidates).View();
      return redirectSuccess;
  }
}