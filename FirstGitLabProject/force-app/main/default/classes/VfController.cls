public class VfController {
    public String Name{get;set;}
 public List<sObject> records{get;set;}
   public List<String> fields{get;set;}
   public List<Job_Shubham__c> details {get; set;}      //for job shubham
    public Boolean showSection{get;set;}
   public VfController(){
       details= [select ID,Name__c,Number_of_Positions__c,Job_Types1__c from Job_Shubham__c];
       
   }

   
    public  PageReference processClick(){
       // Id Id1= ApexPages.currentPage().getParameters().get('Id1');
        records= [Select Full_Name__c,Email__c,Expected_Salary__c,JName__c FROM Candidate_Shubham__c WHERE JName__c=:Name ]; //for candidate ,we fetch from external variable,  and this data send to component
        fields = new List<String>{'Full_Name__c','Email__c','Expected_Salary__c','JName__c'};   //this fields send to component ,means column name
       
            return Null;
       }
   }