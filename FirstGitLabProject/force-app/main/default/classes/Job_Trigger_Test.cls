@isTest
private class Job_Trigger_Test {
    
    @testSetup
    static void createCJob(){
           List<Job_Shubham__c> jobList = new  List<Job_Shubham__c>();
        for(Integer i = 0 ; i<10 ; i++) {

           jobList.add(new Job_Shubham__c(Active1__c=true,Number_of_Positions__c=i,Required_Skills__c='Team Player',Job_Types1__c='Manager'));
       }

insert jobList;
   }
    
    
       
         	 
    public static testMethod void preventDeleteUnitTest()
   {
 
       Test.startTest();
       try{
           List<Job_Shubham__c> jobList=[select Active1__c from Job_Shubham__c where Active1__c=true];
           Delete jobList;
       }
       catch(Exception e)
       {
            String message = e.getMessage();
            System.assert(message.contains('Delete failed.'));
            system.assert(message.contains('This Job is active and can not be deleted'));
       }
       Test.stopTest();
   }
    
    
    
    
     public static testMethod void deleteUnitTest()
       {
           Job_Shubham__c jobObj = new Job_Shubham__c();
           jobObj.Name='Test_Job';
           jobObj.Active1__c=false;
          
          Candidate_Shubham__c cnObj=new Candidate_Shubham__c();
          List<Candidate_Shubham__c> cnList =new List<Candidate_Shubham__c>();
            cnObj.name='ajay';
            cnObj.Job_Shubham__c=jobObj.id;
           cnList.add(cnObj);
           
            Test.startTest();
            Database.SaveResult[] sr = Database.insert(cnList,false);
            Test.stopTest();
		try{
        System.assertEquals(1,sr.size());
                   }
         catch(DmlException e)
         {
              String message = e.getMessage();
             system.assert(message.contains('This Job is not active. You can not apply for this job'));
         }
       }
 
    
       	
            

    }