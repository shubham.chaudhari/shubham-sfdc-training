public class AccountVFController {
   public List<sObject> records{get;set;}     //for manager
   public List<String> fields{get;set;}         //for manager
   public List<Candidate_Shubham__c> candidates{get; set;}
   public List<Job_Shubham__c> details {get; set;}
   public List<Job_Shubham__c> job{get; set;}
     public List<sObject> adminRecords{get;set;}     //for admin
   public List<String> adminFields{get;set;}         //for admin
     public List<Job_Shubham__c> job1{get; set;}
    public List<Candidate_Shubham__c> candidates1{get; set;}
    public List<sObject> deveRecords{get;set;}     //for developer
   public List<String> deveFields{get;set;}         //for developer
     public List<Job_Shubham__c> job2{get; set;}
    public List<Candidate_Shubham__c> candidates2{get; set;}
  
   public AccountVFController(){
       Id id= ApexPages.currentPage().getParameters().get('id');
      
       records= [Select  Name,Job_Types1__c,Qualification_Required__c, Required_Skills__c,Certification_Required__c , Number_of_Positions__c, Salary_Offered__c FROM Job_Shubham__c WHERE Id=:id];   //for job table, and print in VF page using component we pass this record ,first we store in list then pass
       fields= new List<String>{'Name', 'Job_Types1__c','Qualification_Required__c', 'Required_Skills__c', 'Certification_Required__c','Number_of_Positions__c','Salary_Offered__c'};  //fields show job table so this also pass to vf    
       job= [select Job_Types1__c from Job_Shubham__c where Id=:id ];
       candidates=[select First_Name__c, Last_Name__c, Email__c, Status__c, Job_Shubham__c  from Candidate_Shubham__c where Job_Shubham__c=:job];  //for candidate table
      
       
       
       
       Id id1= ApexPages.currentPage().getParameters().get('id1');
       adminRecords= [Select  Name,Job_Types1__c,Qualification_Required__c, Required_Skills__c,Certification_Required__c , Number_of_Positions__c, Salary_Offered__c FROM Job_Shubham__c WHERE Id=:id1];   //for job table, and print in VF page using component we pass this record ,first we store in list then pass
        job1= [select Job_Types1__c from Job_Shubham__c where Id=:id1 ];
       candidates1=[select First_Name__c, Last_Name__c, Email__c, Status__c, Job_Shubham__c  from Candidate_Shubham__c where Job_Shubham__c=:job1];  //for candidate table
       adminFields= new List<String>{'Name', 'Job_Types1__c','Qualification_Required__c', 'Required_Skills__c', 'Certification_Required__c','Number_of_Positions__c','Salary_Offered__c'};  //fields show job table so this also pass to vf    
     
     
		Id id2= ApexPages.currentPage().getParameters().get('id2');
       deveRecords= [Select  Name,Job_Types1__c,Qualification_Required__c, Required_Skills__c,Certification_Required__c , Number_of_Positions__c, Salary_Offered__c FROM Job_Shubham__c WHERE Id=:id2];   //for job table, and print in VF page using component we pass this record ,first we store in list then pass
        job2= [select Job_Types1__c from Job_Shubham__c where Id=:id2 ];
       candidates2=[select First_Name__c, Last_Name__c, Email__c, Status__c, Job_Shubham__c  from Candidate_Shubham__c where Job_Shubham__c=:job2];  //for candidate table
       deveFields= new List<String>{'Name', 'Job_Types1__c','Qualification_Required__c', 'Required_Skills__c', 'Certification_Required__c','Number_of_Positions__c','Salary_Offered__c'};  //fields show job table so this also pass to vf    
     
    
   }
}