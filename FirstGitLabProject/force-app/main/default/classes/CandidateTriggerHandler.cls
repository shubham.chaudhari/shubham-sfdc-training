public class CandidateTriggerHandler {
    public static void checkSalary (List<Candidate_Shubham__c> lstCandidates){       //Task 1 A,  validation rule applied
    
        for(Candidate_Shubham__c candidate : lstCandidates){
            
            if(candidate.Expected_Salary__c==null){
                candidate.addError('You are missing salary field');
            }
        }      
        
    }
   
    
    /*public static void checkDate(List<Candidate_Shubham__c> lstCandidate){					//Task 2 
  
        List<Candidate_Shubham__c> candidateData =new List<Candidate_Shubham__c>();
        for(Candidate_Shubham__c candidateObj: candidateData){
            
           if(candidateObj.Application_Date__c!=system.today()){
                 candidateObj.Application_Date__c=candidateObj.CreatedDate.date();
            }
            
           
            
        } 
        
       
    }*/

    
     public static void checkActiveOrNot(List<Candidate_Shubham__c> lstCandidate){       //Task 1 B
    	
        Map <Id, Job_Shubham__c> JobActive = new Map<Id, Job_Shubham__c>([SELECT Active1__c FROM Job_Shubham__c WHERE Active1__c = false]);
         for(Candidate_Shubham__c candidateObj: lstCandidate){			//iterate list
            if(JobActive.containsKey(candidateObj.Job_Shubham__c)){         //not null,here we search that id where we apply for that job...if that id is present then i.e inactive 
                   
               candidateObj.addError('This Job is not active. You can not apply for this job');
            }
            
        }
    }
    
    
    
  
}