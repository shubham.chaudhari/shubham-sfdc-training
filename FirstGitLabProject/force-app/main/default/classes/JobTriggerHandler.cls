public class JobTriggerHandler {
    
    public static void checkStatus(List<Job_Shubham__c> lstJob){	//Task T3: If Job status is active, It should not get deleted, User should get error “This Job is active and can not be deleted”. 
     
        for(Job_Shubham__c jobObj:lstJob){
            if(jobObj.Active1__c){
                
                jobObj.addError('This Job is active and can not be deleted');    
            }
        }
    }
    
    /*public static void checkVaccancy(List<Job_Shubham__c> lstJob){       //Task T4 and Task T5 
        Set<Id> contactIds = new Set<Id>();   	//for mail  
        Map<Id,Contact> managerContact ;        //for mail
        Map<Job_Shubham__c,String> jobManagerDetails = new Map<Job_Shubham__c,String>();
       
        for(Job_Shubham__c jobObj:lstJob){
            if(jobObj.Hired_Applicants__c == jobObj.Number_of_Positions__c){
                jobObj.Active1__c=false;
                contactIds.add(jobObj.Manager__c);              //managers id store in set whose job is deactive
            }
        }
        managerContact=new Map<Id,contact>([select Name, Email ,Id from contact where id in :contactIds]);  //name email id take and check that id with deactivated jobs id
        for(Job_Shubham__c jobObj:lstJob){
            if(jobObj.Number_of_Positions__c == jobObj.Hired_Applicants__c ){
                String [] address = new List<String>();              //new list assign to array   
                address.add(managerContact.get(jobObj.Manager__c).Email);   //and add all manager mail to that array
                String subject = 'All required candidate Hired'; 
                String Body = 'All required candidate has been hired for this job on :' + jobObj.LastModifiedDate;
                JobTriggerHandler.sentMail(address,subject,Body);
                
                
            }
        }
        
        
    }
    
    
    public static void sentMail(String[]address,String subject,String body){
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = address;
        message.optOutPolicy = 'FILTER';
        message.subject = subject;
        message.plainTextBody = body;
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }	*/    
    
    
    
    public static void activateJob(List<Job_Shubham__c> lstJob){              //Task 6
        
        for(Job_Shubham__c jobObj:lstJob){
            if(jobObj.Number_of_Positions__c > jobObj.Hired_Applicants__c   && !jobObj.Active1__c){
                
                jobObj.Active1__c = true;
            }
        }
        
        
        
        
    }
    
    
}