@isTest

private class TriggerTest{
    
    @testSetup
    static void createData(){
        List<Job_Shubham__c> jobList = new  List<Job_Shubham__c>();
        List<Candidate_Shubham__c> lstCandidate=new List<Candidate_Shubham__c>();
        for(Integer i = 0 ; i<10 ; i++) {
			jobList.add(new Job_Shubham__c(Active1__c=true,Number_of_Positions__c=i,Required_Skills__c='Team Player',Job_Types1__c='Manager'));
           lstCandidate.add(new Candidate_Shubham__c(First_Name__c='test'+i,Last_Name__c='test1'+i,Email__c='test'+i+'@gmail.com',status__c='applied',Job_Shubham__c='a055g0000003P7i',Expected_Salary__c=0));
       }

       insert lstCandidate;
        insert jobList;

   }
 
      
   public static testMethod void checkSalary(){
        Test.startTest();   
       try{
        List<Candidate_Shubham__c> lstCandidate=[SELECT id,First_Name__c,Email__c,Last_Name__c,Job_Shubham__c,Expected_Salary__c,Status__c FROM Candidate_Shubham__c WHERE  Expected_Salary__c=0];
        System.assertEquals(10,lstCandidate.size());
       }
       catch(Exception e){
           String message = e.getMessage();
           System.assert(message.contains('Insert failed.'));
           System.assert(message.contains('Expected Salary field is missing'));
           
          
       }
        Test.stopTest();  
        
    }
    
    
   /* public static testMethod void appDateUnitTest()
    {
           List<Candidate_Shubham__c> cnList=new List<Candidate_Shubham__c>();
        Candidate_shubham__c cnObj=new Candidate_shubham__c();
           cnObj.Application_Date__c=System.today();
           
           cnList.add(cnObj);
           Test.startTest();    
           Database.SaveResult[] sr = Database.insert(cnList,false);
           Test.stopTest();
    }*/
    
    
     public static testMethod void preventDeleteUnitTest()					//for job
   {
 
       Test.startTest();
       try{
           List<Job_Shubham__c> jobList=[select Active1__c from Job_Shubham__c where Active1__c=true];
           Delete jobList;
       }
       catch(Exception e)
       {
            String message = e.getMessage();
            System.assert(message.contains('Delete failed.'));
            system.assert(message.contains('This Job is active and can not be deleted'));
       }
       Test.stopTest();
   }
    
    
    
    public static testMethod void deleteUnitTest()							//for job
       {
           Job_Shubham__c jobObj = new Job_Shubham__c();
           jobObj.Name='Test_Job';
           jobObj.Active1__c=false;
          
          Candidate_Shubham__c cnObj=new Candidate_Shubham__c();
          List<Candidate_Shubham__c> cnList =new List<Candidate_Shubham__c>();
            cnObj.name='ajay';
            cnObj.Job_Shubham__c=jobObj.id;
           cnList.add(cnObj);
           
            Test.startTest();
            Database.SaveResult[] sr = Database.insert(cnList,false);
            Test.stopTest();
		try{
        System.assertEquals(1,sr.size());
                   }
         catch(DmlException e)
         {
              String message = e.getMessage();          
             system.assert(message.contains('This Job is not active. You can not apply for this job'));
         }
       }
   




}