trigger JobTrigger on Job_Shubham__c (before update,before delete,after insert,after update,before insert) {
   
/* For_job_trigger__c obj =  For_job_trigger__c.getValues('00e5g000002UcVa');  //obj create and pass that customsetting data id
    System.debug(obj);
    if(obj.Is_Active__c==true){   */ 
    
    
    
    
    if(Trigger.isBefore && Trigger.isDelete ){
        JobTriggerHandler.checkStatus(Trigger.Old);                 //Task 3 If Job status is active, It should not get deleted, User should get error “This Job is active and can not be deleted
    }
    
    if(Trigger.isBefore && Trigger.isUpdate){                      //Task 4   When the number of Candidates hired equals the number of Positions, Job should be deactivated so no more candidates can apply for that job
        
        //JobTriggerHandler.checkVaccancy(Trigger.New);            //JobTrigger
        JobTriggerHandler.ActivateJob(Trigger.New);				//Task 6
    }
    
		
 //   }
    
}